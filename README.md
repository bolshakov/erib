Идея была сделать что-то типа конструктора шагов для ериба и вынести эту логику из Активности

Основные сущности


```kotlin
// реквесты/респонсы для INIT шага
data class InitialRequest(val param: String)
data class InitialResponse(var param: String)

```


```kotlin
//В каждом состоянии можно выполнить некоторое действие
abstract class ExecutableState<RESULT> {
    @Throws(IOException::class)
    abstract fun execute(mapper: ApiMapper): RESULT
}

//Базовый класс для состояний (сооствует состояниям ериб INIT,FIRST,COMPLETE)
sealed class EribState<Response>: ExecutableState<Response>() {

    open class Init(val request: InitialRequest): EribState<InitialResponse>() {
        override fun execute(mapper: ApiMapper): InitialResponse {
            return mapper.initRequest(request)
        }
    }

    class First(val request: FirstRequest) : EribState<FirstResponse>() // ...

    class Complete(val request: CompleteRequest) : EribState<CompleteResponse>() // ...

}
```

Действия, которые можно выполнять в каждом состоянии
```kotlin
sealed class EribEvent {
    object OnNext : EribEvent()
    object OnForse : EribEvent()
}
```

Это результаты, получаемые на конкретном шаге
```kotlin
sealed class EribResult {
    class Init(val result: InitialResponse) : EribResult()
    class First(val result: FirstResponse) : EribResult()
    class Complete(val result: CompleteResponse) : EribResult()
    class Error(val error: Exception) : EribResult()
}

// может объявить в другом месте
class Next(val result: NextResponse) : EribResult()
```


Обработчик для перехода из Init в First.
Те, если мы находимся в состоянии EribState.Init и нам приходит событие  EribEvent.OnNext, срабатывает данный обработчик
Обработчик должен вернуть новое состояние
Таким образом создается контракт между текущим событием и новым
```kotlin
open class TransitionFromInitToFirst(val mapper: ApiMapper): TransitionTo<EribState.Init, EribState<*>, EribEvent.OnNext, EribResult>() {

    override fun execute(state: EribState.Init, event: EribEvent.OnNext): StateMachine.Transition<EribState<*>, EribResult> {
        return execEribRequest {
            val res = state.execute(mapper)
            StateMachine.Transition(EribState.First(FirstRequest(state.request.param)), EribResult.Init(res))
        }
    }

}

Пример расширения логики существующего обработчика
class InitUpdated(request: InitialRequest): EribState.Init(request)
```

вот так выглядит инициализация базовой машины состояний ериб во ViewModel или в даггере
```kotlin
// начальное состояние
stateMachine.initialState(EribState.Init(InitialRequest("orderId")))

// поддерживаемые обработчики (это список можно дополнять/удалять или заменять элементы)
stateMachine.state(TransitionFromInitToFirst(mapper))
stateMachine.state(TransitionFromFirstToComplete(mapper))
stateMachine.state(TransitionFromCompleteToStatus(mapper))

// слушатель результата обработки
stateMachine.addTransitionListener(createTransitionListener())

// слушатель событий
stateMachine.onEvent { e: EribEvent -> state.postValue(createLoading()) }
```

вот так внутри ViewModel можно получать результаты и обрабатывать их
```kotlin
    when (tr.sideEffect) {
        is EribResult.Init -> state.postValue(createSuccess(tr.sideEffect))
        is EribResult.First -> state.postValue(createSuccess(tr.sideEffect))
        is EribResult.Complete -> state.postValue(createSuccess(tr.sideEffect))
        is EribResult.Error -> state.postValue(createError(tr.sideEffect.error))
    }
```


вот так внутри Активности можно отрисовывать результат
```kotlin
        viewModel.state.observe(this,
            LoadingStateObserver(this, Observer {
                when (it) {
                    is LoadingState.Success -> it.data?.let {
                        viewDelegateAdapter.render(it, this) // таким образом можно подменять логику отрисовки разных шагов, не меняя базовые Активности
                    }
                    is LoadingState.Failure -> it.error.let {
                        errorDelegateAdapter.render(it,this) // таким образом можно подменять логику отрисовки ошибок, не меняя базовые Активности
                    }
                }
            })
        )
```

если потребовалось заменить логику обработки на шаге First, теперь сделать это можно, например так
```kotlin
    class ExtendedPaymentStmViewModel(stateMachine: EribStateMachine, mapper: ApiMapper): BasePaymentStmViewModel(stateMachine, mapper) {

        init {
            // для примера заменяем обработчик
            stateMachine.state(TransitionFromInitToFirstUpdated(mapper))
            // или расширяем логику существующего
            stateMachine.state(TransitionFromInitToFirstExtended(mapper))
            // или добавить новый шаг
            stateMachine.state(TransitionFromInitToNext(mapper))
        }

    }
```