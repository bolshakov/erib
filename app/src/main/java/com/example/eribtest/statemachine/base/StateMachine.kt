package com.example.eribtest.statemachine.base

import java.util.concurrent.atomic.AtomicReference

//
// Created by  on 2019-12-09.
//

/**
 * Обработчик состояния.
 */
abstract class TransitionTo<INSTATE : Any, out OUTSTATE : Any, EVENT : Any, SIDE_EFFECT : Any> {
    abstract fun execute(state: INSTATE, event: EVENT): StateMachine.Transition<OUTSTATE, SIDE_EFFECT>
}

open class StateMachine<STATE : Any, EVENT : Any, SIDE_EFFECT : Any> {

    val states: MutableMap<Pair<Class<*>, Class<*>>, TransitionTo<STATE, STATE, EVENT, SIDE_EFFECT>> = mutableMapOf()
    private val stateRef = AtomicReference<STATE>()
    val onTransitionListeners: ArrayList<(Transition<STATE, SIDE_EFFECT>) -> Unit> = arrayListOf()
    private val onEventListeners = ArrayList<(EVENT) -> Unit>()

    val state: STATE
        get() = stateRef.get()

    fun initialState(initial: STATE) {
        stateRef.set(initial)
    }

    open class Transition<out STATE, out SIDE_EFFECT : Any>(val state: STATE? = null, val sideEffect: SIDE_EFFECT)

    fun <E: EVENT> transition(event: E) {
        notifyOnEvent(event)
        val newState = states[Pair(state::class.java, event::class.java)]?.execute(state, event)
        newState?.state?.let {
            stateRef.set(it)
        }
        newState?.notifyOnTransition()
    }

    inline fun <reified S: STATE, reified E: EVENT> state(transitionTo: TransitionTo<S, STATE, E, SIDE_EFFECT>) {
        @Suppress("UNCHECKED_CAST")
        states[Pair(S::class.java, E::class.java)] = transitionTo as TransitionTo<STATE, STATE, EVENT, SIDE_EFFECT>
    }

    fun addTransitionListener(listener: (Transition<STATE, SIDE_EFFECT>) -> Unit) {
        onTransitionListeners.add(listener)
    }

    fun onEvent(listener: (EVENT) -> Unit) {
        onEventListeners.add(listener)
    }

    private fun notifyOnEvent(event: EVENT) {
        onEventListeners.forEach { it(event) }
    }

    private fun Transition<STATE, SIDE_EFFECT>.notifyOnTransition() {
        onTransitionListeners.forEach { it(this) }
    }
}