package com.example.eribtest.statemachine

import com.example.eribtest.statemachine.base.StateMachine
import java.io.IOException


data class InitialRequest(val param: String)
data class InitialResponse(var param: String)

data class FirstRequest(val param: String)
data class FirstResponse(val param: String)

data class CompleteRequest(val param: String)
data class CompleteResponse(val param: String)

class ApiMapper {
    fun initRequest(request: InitialRequest): InitialResponse {
        return InitialResponse(request.param)
    }

    fun firstRequest(request: FirstRequest): FirstResponse {
        return FirstResponse(request.param)
    }

    fun completeRequest(request: CompleteRequest): CompleteResponse {
        return CompleteResponse(request.param)
    }
}

abstract class ExecutableState<RESULT> {
    @Throws(IOException::class)
    abstract fun execute(mapper: ApiMapper): RESULT
}

sealed class EribState<Response>: ExecutableState<Response>() {

    open class Init(val request: InitialRequest): EribState<InitialResponse>() {
        override fun execute(mapper: ApiMapper): InitialResponse {
            return mapper.initRequest(request)
        }
    }

    class First(val request: FirstRequest) : EribState<FirstResponse>() {
        override fun execute(mapper: ApiMapper): FirstResponse {
            return mapper.firstRequest(request)
        }
    }

    class Complete(val request: CompleteRequest) : EribState<CompleteResponse>() {
        override fun execute(mapper: ApiMapper): CompleteResponse {
            return mapper.completeRequest(request)
        }
    }

}

sealed class EribEvent {
    object OnNext : EribEvent()
    object OnForse : EribEvent()
}

sealed class EribResult {
    class Init(val result: InitialResponse) : EribResult()
    class First(val result: FirstResponse) : EribResult()
    class Complete(val result: CompleteResponse) : EribResult()
    class Error(val error: Exception) : EribResult()
}

class EribStateMachine: StateMachine<EribState<*>, EribEvent, EribResult>()