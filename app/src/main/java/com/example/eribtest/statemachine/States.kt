package com.example.eribtest.statemachine

import com.example.eribtest.statemachine.base.StateMachine
import com.example.eribtest.statemachine.base.TransitionTo
import com.example.eribtest.utils.execEribRequest
import java.io.IOException

//
// Created by  on 2019-12-10.
//

// Обработчик для перехода из Init в First
open class TransitionFromInitToFirst(val mapper: ApiMapper): TransitionTo<EribState.Init, EribState<*>, EribEvent.OnNext, EribResult>() {

    override fun execute(state: EribState.Init, event: EribEvent.OnNext): StateMachine.Transition<EribState<*>, EribResult> {
        return execEribRequest {
            val res = state.execute(mapper)
            StateMachine.Transition(EribState.First(FirstRequest(state.request.param)), EribResult.Init(res))
        }
    }

}


// Обработчик для перехода из First в Complete (с эмуляцией ошибки)
var errCnt = 0

class TransitionFromFirstToComplete(val mapper: ApiMapper): TransitionTo<EribState.First, EribState<*>, EribEvent.OnNext, EribResult>() {

    override fun execute(state: EribState.First, event: EribEvent.OnNext): StateMachine.Transition<EribState<*>, EribResult> {
        return execEribRequest {
            if (0 == errCnt++) {
                throw IOException("Erorr")
            }
            val res = state.execute(mapper)
            StateMachine.Transition(EribState.Complete(CompleteRequest(state.request.param)), EribResult.First(res))
        }
    }

}

// Обработчик для перехода из Complete в Статусный экран (с эмуляцией ошибки)
class TransitionFromCompleteToStatus(val mapper: ApiMapper): TransitionTo<EribState.Complete, EribState<*>, EribEvent.OnNext, EribResult>() {

    override fun execute(state: EribState.Complete, event: EribEvent.OnNext): StateMachine.Transition<EribState<*>, EribResult> {
        return execEribRequest {
            val res = state.execute(mapper)
            StateMachine.Transition(sideEffect = EribResult.Complete(res))
        }
    }

}

// Пример расширения логики существующего обработчика
class InitUpdated(request: InitialRequest): EribState.Init(request)

open class TransitionFromInitToFirstExtended(val mapper: ApiMapper): TransitionTo<InitUpdated, EribState<*>, EribEvent.OnNext, EribResult>() {

    override fun execute(state: InitUpdated, event: EribEvent.OnNext): StateMachine.Transition<EribState<*>, EribResult> {
        return execEribRequest {
            val res = state.execute(mapper)
            StateMachine.Transition(EribState.First(FirstRequest(state.request.param)), EribResult.Init(res))
        }
    }

}

// Пример замещения обрабоччика
class TransitionFromInitToFirstUpdated(mapper: ApiMapper): TransitionFromInitToFirst(mapper) {
    override fun execute(state: EribState.Init, event: EribEvent.OnNext): StateMachine.Transition<EribState<*>, EribResult> {
        return execEribRequest {
            val res = state.execute(mapper)
            StateMachine.Transition(EribState.First(FirstRequest(state.request.param + " new")), EribResult.Init(res))
        }
    }
}