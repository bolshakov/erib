package com.example.eribtest.view.delegates

import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.eribtest.R
import com.example.eribtest.view.ICanShowDialog
import com.example.eribtest.utils.delegates.base.IDelegate

//
// Created by  on 2019-12-10.
//

class ErrorViewDelegate:
    IDelegate<Throwable, ICanShowDialog> {
    
    override fun isForViewType(item: Throwable) = true

    override fun createView(root: ICanShowDialog) = root.getActivity().findViewById<TextView>(R.id.textView)

    override fun bindView(v: View, item: Throwable) {
        Toast.makeText(v.context, item.message, Toast.LENGTH_LONG).show()
    }

}