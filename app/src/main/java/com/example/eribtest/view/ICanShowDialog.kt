package com.example.eribtest.view

import android.app.Activity
import android.view.ViewGroup

//
// Created by  on 2019-12-10.
//

interface ICanShowDialog {
    fun getActivity(): Activity
}