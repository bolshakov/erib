package com.example.eribtest.view

import android.view.ViewGroup

//
// Created by  on 2019-12-10.
//

interface IEribActivity {
    fun getContainer(): ViewGroup
}