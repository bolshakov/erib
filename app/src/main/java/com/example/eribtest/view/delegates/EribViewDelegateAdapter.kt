package com.example.eribtest.view.delegates

import com.example.eribtest.statemachine.EribResult
import com.example.eribtest.utils.delegates.base.BaseDelegateAdapter
import com.example.eribtest.utils.delegates.base.DelegateManager
import com.example.eribtest.view.ICanShowDialog
import com.example.eribtest.view.IEribActivity

//
// Created by  on 2019-12-10.
//

class EribViewDelegateAdapter(delegateManager: DelegateManager<EribResult, IEribActivity>): BaseDelegateAdapter<EribResult, IEribActivity>(delegateManager) {

}

class EribErrorViewDelegateAdapter(delegateManager: DelegateManager<Throwable, ICanShowDialog>): BaseDelegateAdapter<Throwable, ICanShowDialog>(delegateManager) {

}