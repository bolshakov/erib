package com.example.eribtest.view.delegates

import android.view.View
import android.widget.TextView
import com.example.eribtest.R
import com.example.eribtest.statemachine.EribResult
import com.example.eribtest.utils.delegates.base.IDelegate
import com.example.eribtest.view.IEribActivity

//
// Created by  on 2019-12-10.
//

class FirstStepViewDelegate:
    IDelegate<EribResult, IEribActivity> {
    
    override fun isForViewType(item: EribResult) = item is EribResult.First

    override fun createView(root: IEribActivity) = root.getContainer().findViewById<TextView>(R.id.textView)

    override fun bindView(v: View, item: EribResult) {
        val data = item as EribResult.First
        (v as TextView).text = "First  ${data.result.param}"
    }

}