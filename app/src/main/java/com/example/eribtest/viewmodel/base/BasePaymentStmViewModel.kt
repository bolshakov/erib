package com.example.eribtest.viewmodel.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.eribtest.statemachine.*
import com.example.eribtest.statemachine.base.StateMachine
import com.example.eribtest.utils.LoadingState
import com.example.eribtest.utils.createError
import com.example.eribtest.utils.createLoading
import com.example.eribtest.utils.createSuccess
import java.util.concurrent.Executors


open class BasePaymentStmViewModel(val stateMachine: EribStateMachine, val mapper: ApiMapper): ViewModel() {

    val exService = Executors.newSingleThreadExecutor()
    val state: MutableLiveData<LoadingState<EribResult, Throwable>> = MutableLiveData()

    init {
        stateMachine.initialState(EribState.Init(InitialRequest("orderId")))

        stateMachine.state(TransitionFromInitToFirst(mapper))
        stateMachine.state(TransitionFromFirstToComplete(mapper))
        stateMachine.state(TransitionFromCompleteToStatus(mapper))

        stateMachine.addTransitionListener(createTransitionListener())

        stateMachine.onEvent { e: EribEvent -> state.postValue(createLoading()) }

    }

    fun createTransitionListener(): (StateMachine.Transition<EribState<*>, EribResult>) -> Unit =
        { tr: StateMachine.Transition<EribState<*>, EribResult> ->
                when (tr.sideEffect) {
                    is EribResult.Init -> state.postValue(createSuccess(tr.sideEffect))
                    is EribResult.First -> state.postValue(createSuccess(tr.sideEffect))
                    is EribResult.Complete -> state.postValue(createSuccess(tr.sideEffect))
                    is EribResult.Error -> state.postValue(createError(tr.sideEffect.error))
                }
        }


    fun event(event: EribEvent) {
        exService.submit {
            stateMachine.transition(event)
        }
    }

}