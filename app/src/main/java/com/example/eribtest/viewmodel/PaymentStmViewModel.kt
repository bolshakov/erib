package com.example.eribtest.viewmodel

import com.example.eribtest.statemachine.ApiMapper
import com.example.eribtest.statemachine.EribStateMachine
import com.example.eribtest.statemachine.TransitionFromInitToFirstUpdated
import com.example.eribtest.statemachine.TransitionFromInitToFirstExtended
import com.example.eribtest.viewmodel.base.BasePaymentStmViewModel


class PaymentStmViewModel(stateMachine: EribStateMachine, mapper: ApiMapper): BasePaymentStmViewModel(stateMachine, mapper) {

    init {
        // для примера заменяем обработчик
        stateMachine.state(TransitionFromInitToFirstUpdated(mapper))
        // или расширяем логику существующего (пока не доделано)
        stateMachine.state(TransitionFromInitToFirstExtended(mapper))
    }

}