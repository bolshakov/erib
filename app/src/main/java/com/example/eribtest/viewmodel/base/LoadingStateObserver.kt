package com.example.eribtest.viewmodel.base

import androidx.lifecycle.Observer
import com.example.eribtest.utils.LoadingState

//
// Created by  on 2019-12-10.
//

interface ICanShowProgress {
    fun progress(isLoading: Boolean)
}

class LoadingStateObserver<T, E>(val loadingState: ICanShowProgress, val observer: Observer<LoadingState<T, E>>): Observer<LoadingState<T, E>> {
    override fun onChanged(ls: LoadingState<T, E>?) {
        when(ls) {
            is LoadingState.Loading -> loadingState.progress(true)
            else -> {
                loadingState.progress(false)
                observer.onChanged(ls)
            }
        }
    }
}