package com.example.eribtest.utils.delegates.base

import android.util.Log
import android.util.SparseArray


class DelegateManager<DATA, BASE_VIEW> @SafeVarargs
constructor(vararg delegates: IDelegate<DATA, BASE_VIEW>) {

    private val mDelegates: SparseArray<IDelegate<DATA, BASE_VIEW>>

    init {
        mDelegates = SparseArray()
        for (i in delegates.indices) {
            mDelegates.put(i, delegates[i])
        }
    }

    /**
     * Получить тип элемента по его позиции
     * @param data список элементов
     * @param position позиция в списке
     * @return тип элемента
     */
    private fun getItemViewType(data: DATA): Int? {
        for (i in 0 until mDelegates.size()) {
            val delegate = mDelegates.valueAt(i)

            if (delegate.isForViewType(data)!!) {
                return mDelegates.keyAt(i)
            }
        }
        Log.d(TAG, "Can't find viewType for type = $data")
        return null
    }

    /**
     * Получить делегат, отвечающий за обработку конкретного элемента
     * @param data список элементов
     * @param position позиция элемента
     * @return делегат
     */
    internal fun getDelegate(data: DATA): IDelegate<DATA, BASE_VIEW>? {
        val viewType = getItemViewType(data)
        return if (viewType == null) null else mDelegates.get(viewType)
    }

    companion object {

        private val TAG = "DelegateManager"
    }

}
