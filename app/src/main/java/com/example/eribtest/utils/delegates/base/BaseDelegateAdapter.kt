package com.example.eribtest.utils.delegates.base

import android.util.Log


abstract class BaseDelegateAdapter<DATA, BASE_VIEW>(private val mDelegateManager: DelegateManager<DATA, BASE_VIEW>) {

    fun render(data: DATA, baseView: BASE_VIEW) {
        val delegate = mDelegateManager.getDelegate(data)
        if (delegate == null) {
            Log.e(TAG, "Delegate not found for type = $data", IllegalArgumentException())
            return
        }
        val view = delegate.createView(baseView)
        delegate.bindView(view, data)
    }

    companion object {

        private val TAG = "BaseDelegateAdapter"
    }
}
