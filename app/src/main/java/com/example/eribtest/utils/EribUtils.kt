package com.example.eribtest.utils

import com.example.eribtest.statemachine.EribResult
import com.example.eribtest.statemachine.EribState
import com.example.eribtest.statemachine.base.StateMachine
import java.lang.Exception

//
// Created by  on 2019-12-09.
//

fun execEribRequest(work: () -> StateMachine.Transition<EribState<*>, EribResult>): StateMachine.Transition<EribState<*>, EribResult> {
    return try {
        Thread.sleep(1000)
        work.invoke()
    } catch (t: Exception) {
        StateMachine.Transition(sideEffect = EribResult.Error(t))
    }
}