package com.example.eribtest.utils.delegates.base

import android.view.View

/**
 * Спецификация объекта, умеющего создавать и инициализировать вью по типу данных
 * @param <T> тип данных
</T> */
interface IDelegate<DATA, BASE_VIEW> {
    /**
     * Проверить может ли текущий делегат обработать данный элемент
     */
    fun isForViewType(item: DATA): Boolean?


    /**
     * Создать вьюху
     * @param root родительский ViewGroup
     * @return новая вью
     */
    fun createView(root: BASE_VIEW): View

    /**
     * Связать вью с данными
     * @param v вьюха
     * @param item модель данных
     */
    fun bindView(v: View, item: DATA)
}