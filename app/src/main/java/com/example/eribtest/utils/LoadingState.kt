package com.example.eribtest.utils


/**
 * Обертка для результата выполнения некотрой операции (например сетевого запроса)
 *
 * @author Igor Bolshakov on 14/06/2019
 */
sealed class LoadingState<out T, out E> {

    /**
     * Запрос выполняется
     */
    class Loading : LoadingState<Nothing, Nothing>()

    /**
     * Запрос выполнен успешно
     */
    class Success<out T>(val data: T?) : LoadingState<T, Nothing>()

    /**
     * Запрос выполнен с ошибкой
     */
    class Failure<out E>(val error: E) : LoadingState<Nothing, E>()

    inline fun <T1, E1> map(transformData: (T?) -> T1?, transformError: (E) -> E1): LoadingState<T1, E1> {
        return when (this) {
            is Loading -> {
                Loading()
            }
            is Success -> {
                Success(transformData.invoke(this.data))
            }
            is Failure -> {
                Failure(transformError.invoke(this.error))
            }
        }
    }
}

fun createLoading(): LoadingState<Nothing, Nothing> = LoadingState.Loading()
fun <T> createSuccess(data: T): LoadingState<T, Nothing> = LoadingState.Success(data)
fun <E> createError(error: E): LoadingState<Nothing, E> = LoadingState.Failure(error)