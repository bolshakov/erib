package com.example.eribtest

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.Consumer
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.eribtest.statemachine.ApiMapper
import com.example.eribtest.statemachine.EribEvent
import com.example.eribtest.statemachine.EribStateMachine
import com.example.eribtest.viewmodel.PaymentStmViewModel
import com.example.eribtest.viewmodel.base.BasePaymentStmViewModel

import com.example.eribtest.utils.LoadingState
import com.example.eribtest.utils.delegates.base.DelegateManager
import com.example.eribtest.view.ICanShowDialog
import com.example.eribtest.view.IEribActivity
import com.example.eribtest.view.delegates.*
import com.example.eribtest.viewmodel.base.ICanShowProgress
import com.example.eribtest.viewmodel.base.LoadingStateObserver

class ViewModelProviderFactory<VM : ViewModel>



@JvmOverloads constructor(
    private val mViewModelCreator: () -> VM,
    private val mOnCreateHook: Consumer<VM>? = null
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val viewModel = mViewModelCreator.invoke()

        mOnCreateHook?.accept(viewModel)

        return viewModel as T
    }
}

// >> fixme inject from DI
val eribStm = EribStateMachine()
val mapper = ApiMapper()

// делегаты для отрисовки состояний
val viewDelegateManager = DelegateManager(
    FirstStepViewDelegate(),
    InitialStepViewDelegate(),
    CompleteStepViewDelegate()
)
val viewDelegateAdapter = EribViewDelegateAdapter(viewDelegateManager)


// делегаты для отрисовки ошибок
val errorDelegateManager =
    DelegateManager(ErrorViewDelegate())
val errorDelegateAdapter = EribErrorViewDelegateAdapter(errorDelegateManager)
// << fixme inject from DI

class EribActivity : AppCompatActivity(), IEribActivity,
    ICanShowProgress,
    ICanShowDialog {

    val TAG = "EribActivity"
    lateinit var progressContainer: ViewGroup
    lateinit var textView: TextView
    lateinit var next: Button
    lateinit var viewModel: BasePaymentStmViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_erib)

        initViews()
        initViewModel()
    }

    fun initViewModel() {
        viewModel = createViewModel()

        viewModel.state.observe(this,
            LoadingStateObserver(this, Observer {
                when (it) {
                    is LoadingState.Success -> it.data?.let {
                        viewDelegateAdapter.render(it, this)
                    }
                    is LoadingState.Failure -> it.error.let {
                        errorDelegateAdapter.render(it,this)
                    }
                }
            })
        )
    }

    fun createViewModel() = ViewModelProviders.of(this, ViewModelProviderFactory({
        PaymentStmViewModel(
            eribStm, mapper
        )
    })).get(PaymentStmViewModel::class.java)

    fun initViews() {
        textView = findViewById(R.id.textView)

        next = findViewById(R.id.next)
        next.setOnClickListener { viewModel.event(EribEvent.OnNext) }

        progressContainer = findViewById(R.id.progressContainer)
    }

    override fun getContainer() = findViewById<ViewGroup>(R.id.container)
    override fun getActivity() = this

    override fun progress(isLoading: Boolean) {
        progressContainer.visibility = if (isLoading) View.VISIBLE else View.GONE
    }
}
